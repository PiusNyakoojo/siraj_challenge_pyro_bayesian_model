## This is my submission to Siraj Raval's programming challenge.

* The challenge is to create a Bayesian Regression model using Uber's Pyro programming language on a data set of our choice.
* [Details of the challenge](https://www.youtube.com/watch?v=ATaMq62fXno)
